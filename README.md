# wdget2024117

# GitLab CI/CD Pipeline Demo

Welcome to the GitLab CI/CD Pipeline Project! This repository contains a simple web application built with Flask, a micro web framework for Python. The project also includes a YAML file for GitLab CI/CD pipeline configuration.

## About the Project

This project serves as an example of creating a basic web application using Flask and setting up a CI/CD pipeline using GitLab.

### Features

- **Homepage**: Includes a basic homepage with a form for user input.
- **Form Submission**: Allows users to submit a form with their name.
- **Greeting**: Greets the user with their submitted name.
- **GitLab CI/CD Pipeline**: The flask app is deployed using the GitLab CI/CD pipeline.

### To Run the Flask app
#### - pip install flask
#### - python app.py
# GitLab CI/CD Pipeline

This project includes a `.gitlab-ci.yml` file that defines a CI/CD pipeline in GitLab. The pipeline consists of two stages: test and build.

## Pipeline Stages

### Test
- Runs a simple test job to check the Python version.

### Build
- Installs Flask dependencies and starts the Flask application in the background.

## Running the Pipeline

The GitLab CI/CD pipeline is automatically triggered on every push to the repository. You can view the pipeline status and logs in the GitLab CI/CD interface.

## Note:

The last line of this `.gitlab-ci.yml` file is:  `python app.py /dev/null 2>&1 &` , here `/dev/null 2>&1 &` helps to run the pipeline job in background, it tells the terminal to not take any input.

## Troubleshooting Tips
- If you encounter any issues with running the Flask application or the pipeline, ensure that Flask and its dependencies are properly installed and that the necessary ports are not blocked.
- Check the GitLab CI/CD interface for detailed logs and error messages to troubleshoot any pipeline failures.