from flask import Flask, render_template, request

app = Flask(__name__)

# Defining a basic route for the homepage
@app.route('/')
def home():
    return render_template('index.html')

# Defining a route for form submission
@app.route('/submit', methods=['POST'])
def submit():
    if request.method == 'POST':
        # Aform with a single input field named 'name'
        name = request.form['name']
        return f'Hello, {name}!'

if __name__ == '__main__':
    app.run(debug=True)